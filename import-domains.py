#! /usr/bin/env python

import ConfigParser
import MySQLdb


vhost_file = "/etc/postfix/vhosts.txt"

# load config
cfg = ConfigParser.SafeConfigParser()
cfg.read('config/app.ini')

# set up database
db = MySQLdb.connect(cfg.get('database', 'host'), cfg.get('database', 'user'), cfg.get('database', 'pass'), cfg.get('database', 'name'))


f = open(vhost_file, 'r')

for line in f.readlines():
    dom = line.rstrip("\r\n")    

    # check to see if domain exists in database already
    sql = "select id from domains where url = %s" 
    c = db.cursor()
    c.execute(sql, (dom,))

    # insert domain if non existent
    if c.rowcount == 0:
        sql2 = "insert into domains (url) values (%s)"
        c2 = db.cursor()
        c2.execute(sql2, (dom,))
        db.commit()
        c2.close()

    c.close()

db.close()
