#! /usr/bin/env python3

import os
import sys
import email
import mailbox
import ConfigParser
import MySQLdb
import parsedatetime

from datetime import datetime

def unicode_it(data):
    try:
        return unicode(data, errors="replace").strip()
    except TypeError as e:
        return u''

def get_box_id(domain=None, db=None):
    if db is None:
        raise Exception('No vaild database connection available.')

    if domain is None:
        raise Exception('Invalid domain supplied when trying to retrieve domain id.')

    c = db.cursor()
    c.execute('select id from domains where url = %s and status = 1', (domain,))
    row = c.fetchone()

    if row is None:
        raise Exception('Could not pull ID for domain ' + domain + '. Are the mail domains set up correctly?')

    id = int(row[0])
    c.close

    return id


def read_message(msg=None):
    data = {'to': None,
            'from': None,
            'subject': None,
            'body': None,
            'attachments': {}
            }


    if msg is None:
        raise Exception('Could not parse a blank message.')

    for key, value in msg.items():
        data[key.lower()] = unicode_it(value)

    if msg.is_multipart():
        print ('    Message is multipart, proccessing the parts.')
        partcount = 1
        for part in msg.get_payload():
            if part.is_multipart():
                data['part_' + str(partcount)] = read_message(part)
                continue
            if part.get('Content-Disposition') is None:
                data['contents'] = unicode_it(part.get_payload())
            else:
                if part.get('Content-Disposition').startswith('attachments'):
                    data['attachments'][part.get_filename()] = {
                            'data': part.get_payload(decode=True),
                            'mime': part.get_content_type()
                        }
            partcount += 1

    else:
        print ('   Message is not multipart, processing all contents.')
        data['body'] = unicode_it(msg.get_payload())

    return data

def add_msg_to_db(domain_id=None, msg=None, db=None):
    if domain_id is None:
        raise Exception('Bad domain ID supplied.')

    if msg is None:
        raise Exception('Bad message supplied')

    sql = 'INSERT INTO messages VALUES (NULL, %s, %i, %s, %s, %s, %s, %s, %s)'

    # Python has this rigmarole with dates.
    cal = parsedatetime.Calendar()
    time_struct, parse_status = cal.parse(msg['date'])
    msg_dt = datetime(*time_struct[:6])

    data = (msg['message-id'], domain_id, str(msg_dt), msg['to'], msg['from'], msg['subject'], msg['body'])
    print(data)
    #c = db.cursor()
    #c.execute(sql, data)

cfg = ConfigParser.ConfigParser()
cfg.read(['config/app.ini'])

# get path to mailboxes
mail_home = cfg.get('fs', 'mail_home')

# set up database.
# 20170215, nic: warning the database parms here are currently
# hardcoded to point to the dev version of the database.
db = MySQLdb.connect(host=cfg.get('database', 'host'),
		     user=cfg.get('database', 'user'),
		     passwd=cfg.get('database', 'pass'),
                     db='dev_postoffice')

for box_name in os.listdir(mail_home):
    box_path = mail_home + '/' + box_name

    if os.path.isdir(box_path):
        box_id = get_box_id(box_name, db)

    	if box_id is None:
		raise Exception('No domain ID returned from the ID lookup. Are the mail domains set up correctly?')

	print('Processing ' + box_path + ' (ID: '  + str(box_id) + ') ...')
        box = mailbox.Maildir(box_path, factory=email.message_from_file)

        for envelope in box.items():
            if envelope is not None:
               msg = read_message(envelope[1])
               add_msg_to_db(box_id, msg, db)
               sys.exit(1)
            else:
              print('    Discarding message for invalid email.')
              box.discard(msg[0])

	box.close()
	box = None;
    else:
        print('Skipping ' + box_path + '...')
