#! /usr/bin/env python


from distutils.core import setup

setup(name='postoffice',
      version='1.0',
      description='easier email management',
      author='Nic Barr',
      author_email='theothernic@outlook.com',
      packages=['MySQLdb', 'parsedatetime'])